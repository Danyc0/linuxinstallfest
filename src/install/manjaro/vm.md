# Installing Manjaro in a Virtual Machine

This guide will help you install Manjaro in a Virtual Machine. It is written
under the assumption you are using Windows. The VirtualBox installation for
MacOS should be pretty much the same. For Linux distributions, install
VirtualBox through your package manager.

You will need:
* [The Manjaro ISO](https://manjaro.org/downloads/official/gnome/)

This guide assumes you have VirtualBox installed. If you don't, it means
you skipped the [Virtual Machine Pre-install](./../../vm-preinstall.md) post!
Head back there and follow those steps.


## Installing Manjaro

In VirtualBox, go to Machine -> New (or just press `CTRL+N`). Input the name
you want for your VM (good practice is to include what OS it is), select the
type as `Linux` and the version as `Arch Linux (64-bit)`.

![../../images/manjaro/vm-create-os.png](../../images/manjaro/vm-create-os.png)

Next, you'll be asked to select the memory size. This depends on what specs
**your** machine has. Manjaro recommends a minimum of 1 GB RAM (1024 MB). You
can allocate it more, but don't worry too much, these settings can be
modified later too! We would recommend 2 or 4GB, but you can leave the
default 1 GB and if you find the machine is too sluggish you can revisit the
settings.

![../../images/manjaro/vm-create-mem.png](../../images/manjaro/vm-create-mem.png)

We are then going to create a virtual hard disk now (default option), and the
Hard disk file type should be VDI (VirtualBox Disk Image). Choose to have the
disk dynamically allocated. Next, choose the size of the hard disk. Manjaro
recommends 30GB.

![../../images/manjaro/vm-create-hdd.png](../../images/manjaro/vm-create-hdd.png)

Done! You should now see your new VM in the list:


![../../images/manjaro/created.png](../../images/manjaro/created.png)

Selecting the new VM (called Manjaro in this example), click Settings (or
press `CTRL+S`) and go to the Storage option. Under `Controller: IDE`, click
the `Empty` disk. Click the blue disk next to the Optical Drive dropdown, and
select `Choose a disk file`. Navigate to where you saved the Manjaro ISO and
open it. 

![../../images/manjaro/insert-iso.png](../../images/manjaro/insert-iso.png)

While still in Settings, go to the Display option and **change** the graphics
controller to be `VBoxSVGA`. _If you miss this step, the VM will most likely
be stuck in a boot loop._

![../../images/manjaro/display-settings.png](../../images/manjaro/display-settings.png)

Back to the list of VMs, select the Manjaro VM and press Start. Your VM
should start. You may be asked to confirm the startup disk, select the
Manjaro ISO and press Start.

![../../images/manjaro/confirm-startup.png](../../images/manjaro/confirm-startup.png)

Your VM is now booting! Press `Enter` or wait for the timer to reach 0:

![../../images/manjaro/first_boot.png](../../images/manjaro/first_boot.png)

Manjaro Hello will greet you on the screen: 
![../../images/manjaro/manjaro_hello.png](../../images/manjaro/manjaro_hello.png)

Press `Launch installer`. Proceed with choosing the language, time zone and
keyboard layout.

Next, we will take care of partitioning. Choose Erase disk with no swap. You
may also choose to tick the box to encrypt the system. Leave any other settings
as they are.

![../../images/manjaro/partitions.png](../../images/manjaro/partitions.png)

Now we'll create the main user. Fill in your name, the name you want to use
to login (this will be your **username**) and a name for the machine (this
will be the name the VM will be seen as over a network). Next, choose your
account password. Make it a good one! You can then also choose the
administrator account password (which is the `root` user), or choose to 
use the first supplied password for the admin account too.

![../../images/manjaro/users.png](../../images/manjaro/users.png)

You can then choose to install LibreOffice or FreeOffice (the Linux
alternatives to Microsoft Office).

When you're ready, press Install! You will be prompted that disk changes
will be made in order to install Manjaro. Confirm by pressing Install now.
Wait for the installation to finish.

_Note: keep an eye on it, my screen went blank due to the default power saving
settings and never recovered. You can change these by right-clicking on the
desktop -> Settings -> Power and modify Blank Screen to _Never_ and toggle
off Suspend & Power Button. If you get the blank screen and have only given
the VM 1GB of RAM, it might be a good idea to give it a bit more too, for the
installation. You can do this from Settings -> System, once the machine has
been powered off. But if the blank screen happens to you too, don't worry!
Go to File -> Close and choose Power Off the Machine. Then you can restart the
installation process. It's a VM, so nothing on your actual system is at risk!_

The installation will finish. Do not check the restart now button. Click done.

You can now close the VM: go to File -> Close and choose Power Off the Machine.
From the VMs list, having selected the Manjaro VM, click Settings and choose
the Storage option. Select the Manjaro ISO under `Controller: IDE` and click the
remove button at the bottom (blue square with a red x). Confirm you want to
remove the disk. **This step is very important. If you fail to remove
the ISO, the VM will boot from the Live CD, not from the VM disk!**

![../../images/manjaro/remove-iso.png](../../images/manjaro/remove-iso.png)

You can now go start your VM! It's ready! Manjaro Hello should greet you again.
You can disable its Launch at startup functionality (toggle the button in the
right bottom corner).

Enjoy your new Manjaro VM!

![../../images/manjaro/neofetch.png](../../images/manjaro/neofetch.png)

Now head over to the [Post Installation](../../post-install.md) guide to update your
system and install some useful software and the [VM Post
Install](../../vm-post-install-ga.md) guide to learn of some useful features for
your VM!