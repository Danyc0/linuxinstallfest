# Replacing current OS with Ubuntu

<span style="color:red; font-weight:bold">Warning: This method will delete your Windows partition. Please make sure
you have made a backup of all the data you do not want to loose.</span>

## Step 1 (Preparation)

Before starting, download Ubuntu Desktop LTS 20.04 from
<https://ubuntu.com/#download> or get it directly from [here](https://releases.ubuntu.com/20.04.1/ubuntu-20.04.1-desktop-amd64.iso).
LTS stands for Long Term Support, so this system will be supported for at
least the next 5 years without an upgrade to the next version.

You'll get an ISO file, which is a disk image. You need to flash this to a
USB drive, using an imager tool. The Raspberry Pi foundation keeps an
up-to-date list of [imagers for Windows](https://www.raspberrypi.org/documentation/installation/installing-images/windows.md),
however, we recommend [Etcher](https://www.balena.io/etcher/). [Rufus](https://rufus.ie/) is another good ISO burner.


In Etcher:
- Select your ISO file
- Select your USB drive (plug it in!)
- Then flash!

![Etcher](../../images/ubuntu/etcher.png)

## Step 2 (Boot)

To boot into the live installation media, plug your USB into your
computer/laptop, and reboot - you should be able to boot off it with no
problems.

Once boot has finished, you should be presented with the installer!

![Installer](../../images/ubuntu/installer.png)

Click "Try Ubuntu" to be able to play around with a live preview (or need to
use one of the bundled tools to help with installation), otherwise, go direct
to "Install Ubuntu".

If you click "Try Ubuntu", you can find the installer later by clicking the icon on the desktop:

![Start Installer](../../images/ubuntu/installer-shortcut.png)

## Step 3 (Installation)

### Select your language

![Installer Language](../../images/ubuntu/installer-language.png)

### Select your keyboard layout

The auto-detect keyboard should walk you through finding out exactly what
layout you have if you're not sure.

![Installer Keyboard](../../images/ubuntu/installer-keyboard.png)

### Connect to the internet

Choose the wifi you want to connect to:

![Wifi](../../images/ubuntu/wifi.png)

### Select software

In most cases you want a "Normal installation" with all the utilities -
however, if you're working with less disk space, or want to manually install
only the tools you want later, then go with a "Minimal installation".

If you have an internet connection, then select "Download updates" - it makes
the install process a little longer, but ensures that everything will be
properly up to date.

The "Install third-party software" is slightly more complex. In most cases,
you should tick it, and attempt an install - if something breaks and doesn't
work, for issues related to drivers, then you can try again, disabling this
step, and instead trying to install the drivers and codecs after the install
is fully complete.

![Installer Software](../../images/ubuntu/installer-software.png)

### Choose installation type

**Be careful at this step! After you click "Install Now" the install process
will begin!**

For a pure install, click "Erase disk and install Ubuntu".

![Installer Type](../../images/ubuntu/installer-type.png)

At this step, you can also choose to encrypt your disk, by selecting
"Advanced features" and selecting both "Use LVM" and "Encrypt the new Ubuntu
installation". However, this is optional, and requires you to input your
password at each boot.

![Installer Encrypt drives](../../images/ubuntu/installer-encrypt.png)

After you click "Install Now", you'll be asked if you want to proceed with the partitioning layout you've selected:

![Installer Partitioning](../../images/ubuntu/installer-partitioning.png)

Verify the changes, and then click "Continue". Note that your partitions
*will* look different depending on your setup.

### Select your timezone

![Installer Location](../../images/ubuntu/installer-location.png)

### Setup your account

You need to pick:

- Your name (used in the display manager to greet you, etc)
- Your computer's name (the hostname used on networks, pick something unique and recognizable)
- Your username (used to login, appears in shell prompts, etc)
- Your password (standard password guidelines apply, if you want something easy to remember and secure, try [diceware](https://en.wikipedia.org/wiki/Diceware))

![Installer Account](../../images/ubuntu/installer-whoami.png)

### Wait!

Now just wait for the installer to complete!

![Installer Wait](../../images/ubuntu/installer-wait.png)

Once it's completed, follow the prompts to shutdown, remove the installation
media, and restart your computer. When you startup, you should be booted into
Ubuntu!

Now head over to the [Post Installation](../../post-install.md) guide to update your
system and install some useful software.
