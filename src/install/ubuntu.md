# Ubuntu install

Ubuntu is one of the go-to first installs for newcomers to Linux. It's easy
to setup, easy to maintain, and has excellent support for third-party
programs, as well as some of the best driver setup and support out there.

The following section will give you the instructions on how to install Ubuntu
on your computer.

Please choose the appropriate method of installation.

<span style="color:red; font-weight:bold">Warning!</span>
If you are installing it as a replacement OS or **dualboot**, please make sure
you have a **backup** of any data you do not want to lose. We are providing the
instructions as they are, and **we are not responsible for any data loss that
might occur. Use at your own risk**.


## Choose from:

- [Just Ubuntu](./ubuntu/pure.md) -- replace your current OS with
  Ubuntu; this will **completely** erase data on your computer; **backup**
  anything you need. 
- [Dualboot](./ubuntu/dualboot.md) -- install Ubuntu alongside an
  existing Windows install; **backup** any important data, in case something
  goes wrong 
- [VM](./ubuntu/vm.md) -- install Ubuntu in a Virtual Machine; there
  will be no data loss; instructions are for Windows 
- [WSL](./ubuntu/wsl.md) -- instructions on how to enable Windows
  Subsystem for Linux and get Ubuntu running; no data loss expected 

