# Speedtest 

Speedtest is a command that tests how fast your internet is, this application is a command-line interface (CLI) for the website [speedtest.net](https://speedtest.net).

## Installation
Manjaro:

     sudo pacman -S speedtest-cli

Ubuntu:

    sudo apt install python3 speedtest-cli

## Running
To verify your installation, run the following command

    speedtest --version


In order to run speedtest-cli, run the following command via terminal.

    speedtest

## Uninstalling
Manjaro:

     sudo pacman -Rs speedtest-cli

Ubuntu:

    sudo apt remove speedtest-cli
